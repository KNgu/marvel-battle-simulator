<?php
include("Class/Avengers.php");

function battle($t1, $t2)
{
    $currT1 = $t1->nextAlive();
    $currT2 = $t2->nextAlive(); 
    
    $round = 0;
    
    while($t1->remaining() > 1 and $t2->remaining() > 1)
    {
        $round ++;
        
        echo "============================================================\nRound: " . $round . "\n";
        
        echo "[" . $t1->getTeamName() . "]" . $currT1->getName() . " versus [". $t2->getTeamName() . "]" . $currT2->getName() . "\n"; 
        
        echo "[" . $t1->getTeamName() . "]" . $currT1->getName() . " attacked [". $t2->getTeamName() . "]" . $currT2->getName() . " for " . $currT2->gotAttacked($currT1->attack()) . " damage!\n";
        
        if($currT2->isDead())
        {
            echo "[" . $t1->getTeamName() . "]" . $currT1->getName() . " killed [". $t2->getTeamName() . "]" . $currT2->getName() . "\n"; 
            
            $currT2 = $t2->nextAlive();
        }
        
        echo "[" . $t1->getTeamName() . "]" . $currT1->getName() . " attacked [". $t2->getTeamName() . "]" . $currT2->getName() . " for " . $currT1->gotAttacked($currT2->attack()) . " damage!\n";
        
        if($currT1->isDead())
        {
            echo "[" . $t2->getTeamName() . "]" . $currT2->getName() . " killed [". $t1->getTeamName() . "]" . $currT1->getName() . "\n"; 
            
            $currT1 = $t1->nextAlive();
        }
        
        echo "============================================================\n";
    }
    
    $str = ($t2->remaining() < 1) ? $t1->getTeamName() : $t2->getTeamName() . " has won!\n";
    
    echo $str;
}

$t1 = new Avengers(100, "A");
$t2 = new Avengers(100, "B");

battle($t1, $t2);

?>