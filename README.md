#Marvel Battle Simulation

This is a submission for Warren Douglas' programming test for the back-end programmer position.

### Version
1.0.0

### Installation

```sh
$ git clone https://KNgu@bitbucket.org/KNgu/marvel-battle-simulator.git
$ cd marvel-battle-simulator/
$ php war.php
```

### File Documentation

* war.php - Main file
* /Class/Hero/Abstracts/HeroAbstract.php - Abstract class for units
* /Class/Hero/Heroes.php - Defines all heroes, extends HeroAbstract.php
* /Class/Avengers.php - Defines team consisting of Heroes