<?php
abstract class Hero
{
    protected $sName;
    protected $iHP;
    protected $iDef;
    protected $iDmg;
    protected $iSpec;
    
    function setName($n)
    {
        $this->sName = $n;
    }
    
    function setHP($n)
    {
        $this->iHP = $n;
    }
    
    function setDef($n)
    {
        $this->iDef = $n;
    }
    
    function setDmg($n)
    {
        $this->iDmg = $n;
    }
    
    function setSpec($n)
    {
        $this->iSpec = $n;
    }
    
    function getName()
    {
        return $this->sName;
    }
    
    function getHP()
    {
        return $this->iHP;
    }
    
    function getDef()
    {
        return $this->iDef;
    }
    
    function getDmg()
    {
        return $this->iDmg;
    }
    
    function getSpec()
    {
        return $this->iSpec;
    }
    
    function isDead()
    {
        return ($this->iHP <= 0) ? true : false;
    }
    
    function attack()
    {
        return (rand(0, 100) <= $this->iSpec) ? floor(rand(1, $this->iDmg) * 1.75) : floor(rand(1, $this->iDmg));
    }
    
    function gotAttacked($dmg)
    {
        $d = (rand(0, $this->iDef));
        
        $this->iHP -= $dmg - $d;
    
        return $d;
    }
    
}

?>