<?php
include("Abstracts/HeroAbstract.php");

class Thor extends Hero
{
    public function __construct()
    {
        $this->setName("Thor");
        $this->setDmg(10);
        $this->setDef(5);
        $this->setHP(10);
        $this->setSpec(25);
    }
}

class Captain_America extends Hero
{
    public function __construct()
    {
        $this->setName("Captain America");
        $this->setDmg(6);
        $this->setDef(8);
        $this->setHP(10);
        $this->setSpec(55);
    }
}

class Iron_Man extends Hero
{
    public function __construct()
    {
        $this->setName("Iron Man");
        $this->setDmg(12);
        $this->setDef(3);
        $this->setHP(4);
        $this->setSpec(45);
    }
}


class Hawkeye extends Hero
{
    public function __construct()
    {
        $this->setName("Hawkeye");
        $this->setDmg(7);
        $this->setDef(4);
        $this->setHP(6);
        $this->setSpec(75);
    }
}

class Hulk extends Hero
{
    public function __construct()
    {
        $this->setName("Hulk");
        $this->setDmg(10);
        $this->setDef(1);
        $this->setHP(15);
        $this->setSpec(10);
    }
}

class Black_Widow extends Hero
{
    public function __construct()
    {
        $this->setName("Black Widow");
        $this->setDmg(12);
        $this->setDef(3);
        $this->setHP(4);
        $this->setSpec(55);
    }
}
?>