<?php

include("Class/Hero/Heroes.php");

class Avengers
{
    private $sName;
    private $aTeam;
    private $iUnits;
    
    public function __construct($units, $name) //assemble
    {
        $this->sName = $name;
        $this->iUnits = $units-1;
        $this->aTeam = array();
        
        $arrHeroes = array("Thor", "Captain America", "Iron Man", "Hawkeye", "Hulk", "Black Widow");
        for($i = 0; $i <= $units-1; $i++)
        {
            $k = mt_rand(0, count($arrHeroes) -1);
            
            switch($arrHeroes[$k])
            {
                case "Thor":
                    $this->aTeam[] = new Thor();
                    break;
                case "Captain America":
                    $this->aTeam[] = new Captain_America();
                    break;
                case "Iron Man":
                    $this->aTeam[] = new Iron_Man();
                    break;
                case "Hawkeye":
                    $this->aTeam[] = new Hawkeye();
                    break;
                case "Hulk":
                    $this->aTeam[] = new Hulk();
                    break;
                case "Black Widow":
                    $this->aTeam[] = new Black_Widow();
                    break;
            }
        }
    }
    
    function getTeamName()
    {
        return $this->sName;
    }
    
    function listTeam()
    {
        foreach($this->aTeam as $k => $v)
        {
            echo $v->getName() . "\n";   
        }
    }
    
    function remaining()
    {
        $ret = 0;
        foreach($this->aTeam as $k => $v)
        {
            if(!$v->isDead())
            {
                $ret += 1;
            }
        }
        return $ret;
    }
    
    function nextAlive()
    {
        foreach($this->aTeam as $k => $v)
        {
            if(!$v->isDead())
            {
                return $v;
            }
        }
        return NULL;
    }
}

?>